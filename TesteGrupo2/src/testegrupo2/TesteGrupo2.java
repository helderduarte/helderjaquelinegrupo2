
package testegrupo2;

import java.io.File;
import java.io.IOException;

/**
 * Teste Git
 * @author helde
 * @author jackie
 */
public class TesteGrupo2 {

    /**
     * menu
     */
    public static void main(String[] args) throws IOException {
        Macros m = new Macros();
        int opcao = 0;
        while (opcao != 7) {

            System.out.println("\nTrabalho Helder e Jackie: \n"
                    + "1 - Temp Maxima de uma cidade" + "\n"
                    + "2 - Temp Media de uma cidade" + "\n"
                    + "3 - Temp máxima de um dia" + "\n"
                    + "4 - Temp minima total" + "\n"
                    + "5 - Sair"
            );
            opcao = m.GetInt("Opção: ");




            switch (opcao) {
                case 1:
                    tempMaxima();
                    break;
                case 2:
                    tempMedia();
                    break;
                case 3:
                    tempMaxDia();
                    break;
                case 4:
                    tempMinTotal();
                    break;
                case 5:
                    m.PL("Saindo ...");
                    break;
                default:
                    m.PL("Digite uma opção válida (1-7)");
                    break;
            }
        }
    }
    public static void tempMaxima() throws IOException {
        Ficheiro ficheiro = new Ficheiro();
        Macros m = new Macros();
        File f = new File("temperaturas");
        String leitura = ficheiro.lerFicheiro("temperaturas");
        String [] linha = leitura.split("\n");
        int cidades=linha.length, tempmaxima=0, temp=0;
        String cidade="";
        
        cidade = m.GetString("Introduza a cidade que pretende saber a temp máxima");
        for (int i = 0; i < cidades; i++) {
                String[] tempes = linha[i].split("-");
                if(cidade==tempes[0]){
                m.PL("A temp máxima foi: "+ tempes[1]);}
                
        }
      
        m.pressioneEnter();
    }
    
    public static void tempMedia() throws IOException {
        Ficheiro ficheiro = new Ficheiro();
        Macros m = new Macros();
        File f = new File("temperaturas");
        String leitura = ficheiro.lerFicheiro("temperaturas");
        String [] linha = leitura.split("\n");
        int cidades=linha.length, tempmedia=0, temp=0;
        String cidade="";
        
        cidade = m.GetString("Introduza a cidade que pretende saber a temp máxima");
        for (int i = 0; i < cidades; i++) {
                String[] tempes = linha[i].split("-");
                if(cidade==tempes[0]){
                tempmedia=(Integer.parseInt(tempes[1])+Integer.parseInt(tempes[2])+Integer.parseInt(tempes[3])+Integer.parseInt(tempes[4])+Integer.parseInt(tempes[5])+Integer.parseInt(tempes[6])+Integer.parseInt(tempes[7])+Integer.parseInt(tempes[8])+Integer.parseInt(tempes[9])+Integer.parseInt(tempes[10]))/10;
                m.PL("A temp média para a cidade introduzida foi: "+ tempmedia);
                }
                
        }
      
        m.pressioneEnter();
    }
    
    public static void tempMaxDia() throws IOException {
        
        
    }
    
    public static void tempMinTotal() throws IOException {
        
        
    }
}
