package testegrupo2;

import java.util.Scanner;

/**
 * Contem varios metodos que permitem encurtar o codigo,
 * tais como GetInt, PL, GetString entre outras.
 */
public class Macros {
    
    /**
     * Metodo que permite obter os numeros introduzidos pelo
     * utilizador, mostrando primeiro uma mensagem e depois lendo o número.
     * Se não for número, volta a pedir.
     * @param Message Texto que pretendemos imprimir para o utilizador.
     * @return Retorna o numero introduzido pelo utilizador.
     */
    public int GetInt(Object Message) {
        boolean valido = false;
        int inteiro = 0;
        while (!valido) {
            System.out.print(Message);
            String n = scan.next();

            try {
                inteiro = Integer.parseInt(n);
                valido = true;
            }
            catch (Exception e) {
            }
        }
        return inteiro;
    }

   
    public void PL(Object escrever) {
        System.out.println(escrever);
    }

    public void P(Object escrever) {
        System.out.print(escrever);
    }

  
    public Scanner scan = new Scanner(System.in);

   
    public static String GetString(Object Message) {
        System.out.print(Message);
        return (new Scanner(System.in)).next();
    }

    
    public void pressioneEnter() {
        P("Pressione ENTER para continuar.");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {}
    }

}
