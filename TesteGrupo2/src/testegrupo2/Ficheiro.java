
package testegrupo2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Contem todos os metodos inerentes aos ficheiros,
 * tal como a criacao de um ficheiro, escrever
 * no ficheiro, ler ou apagar um ficheiro e verificar
 * se um ficheiro existe.
 */
public class Ficheiro {
     
    private String nomeFx;
    private Macros m = new Macros();

    public Ficheiro(){}
    
    public String lerFicheiro(String nomeFx) throws IOException {

        File ficheiro = new File(nomeFx);

        BufferedReader br = new BufferedReader(new FileReader(ficheiro));

        String st;
        String texto = "";
        while ((st = br.readLine()) != null) {
            texto = texto + st + "\n";

        }

        br.close();
        return texto;
    }
}
